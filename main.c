#include "numbers.h"
#include <stdio.h>
#include <stdlib.h>

void app(void){

	int choix = 0;

	do{
		show_menu();
		printf(">");
		scanf("%d", &choix);

		switch(choix){
			case 1:
				do_operation(choix);
				break;
			case 2:
				do_operation(choix);
				break;
			case 3:
				do_operation(choix);
				break;
			case 4:
				do_operation(choix);
				break;
			case 5:
				do_operation(choix);
			case 6:
				do_operation(choix);
			case 7:
				do_operation(choix);
			case 8:
				do_operation(choix);
			case 9:
				break; //Quitter le programme

			default:
				printf("Options incorrecte ! \n");
				break;


		}

	}
	while(choix != 9);
}



int main(void){

	app();

	return 0;
}
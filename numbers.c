#include "numbers.h"
#include <stdio.h>
#include <stdlib.h>
#define NOMBRE 1000

void show_menu(void){

	printf("1: Addition\n2: Soustraction\n3: Multiplication\n4: Division\n5: Egalement\n6: Inegale\n7: Inferieur ou egalement\n8: Superieur ou egalement\n9: Quitter le programme\n");
	printf("-----------------------\n");

}

void do_operation(int choix){


	int nb1 = 0, nb2 = 0;
	int res = 0;

	do{
		printf("Entrez un premier nombre:\n");
		scanf("%d", &nb1);

		printf("Entrez un deuxieme nombre:\n", nb2);
		scanf("%d", &nb2);

		if ( nb1 < -NOMBRE || nb1 > NOMBRE || nb2 < -NOMBRE || nb2 > NOMBRE)
			printf("Sasie fausse prendre un nombre entre -1000 et 1000 \n");
		else
			if(choix == 1) res = addition(nb1, nb2);
			else if (choix == 2) res = soustraction(nb1, nb2);
			else if (choix == 3) res = multiplication(nb1, nb2);
			else if (choix == 4) res = division(nb1, nb2);
			else if (choix == 5) res = egale(nb1,  nb2);
			else if (choix == 6) res = inegale(nb1, nb2);
			else if (choix == 7) res = infouegale(nb1, nb2);
			else if (choix == 8) res = supouegale(nb1, nb2);
			
			printf("------------\n");
			printf("Resulatat calule %d\n", res);
			printf("------------\n");
	}	
	while(nb1 < -NOMBRE || nb1 > NOMBRE || nb2 < -NOMBRE || nb2 > NOMBRE);
		
}


int addition(int a, int b){

	return a + b;
}

int soustraction(int a, int b){

	return a - b;
}

int multiplication(int a, int b){

	return a * b;
}

int division(int a, int b){

	if ( b == 0){
		printf("Division par 0 impossible !\n");
		exit(-1);
	}
	return a / b;
}

int egale(int a, int b){

	return a == b;
}

int inegale(int a, int b){

	return a != b;
}

int infouegale(int a, int b){

	return a <= b;
}

int supouegale(int a, int b){

	return a >= b;
}
